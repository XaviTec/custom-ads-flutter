# custom_ads

Es un Proyecto para colocar publicidad propia mediante Imagen y URL para redireccionar

## Getting Started

Este Proyecto tiene tres formas de mostrar el banner y el tiempo de ejecución se puede personalizar al igual que su tamaño 


1. Es mediante un banner estático que se puede colocar en el lugar que uno desee
<img src="https://gitlab.com/XaviTec/custom-ads-flutter/-/raw/main/assets/1.png" height="600px">

2. Es mediante un panel que aparece desde la parte inferior del dispositivo
<img src="https://gitlab.com/XaviTec/custom-ads-flutter/-/raw/main/assets/2.png" height="600px">

3. Es por medio de un modal
<img src="https://gitlab.com/XaviTec/custom-ads-flutter/-/raw/main/assets/3.png" height="600px">