import 'dart:async';

import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

enum AdsType { normal, modal, panel }

class CustomAds extends StatefulWidget {
  final double height;
  final Duration duration;
  final BorderRadius borderRadius;
  final EdgeInsets padding;
  final String launchUrl;
  final String imageUrl;
  final AdsType adsType;
  const CustomAds({
    Key? key,
    this.height = 100,
    this.duration = const Duration(seconds: 30),
    this.borderRadius = BorderRadius.zero,
    this.padding = const EdgeInsets.all(15.0),
    required this.launchUrl,
    required this.imageUrl,
    this.adsType = AdsType.normal,
  }) : super(key: key);

  @override
  State<CustomAds> createState() => _CustomAdsState();
}

class _CustomAdsState extends State<CustomAds> {
  bool status = false;
  late Timer _timer;

  @override
  void initState() {
    super.initState();
    timerAds(widget.duration);
    switch (widget.adsType) {
      case AdsType.modal:
        Future.delayed(const Duration(seconds: 0)).then((_) {
          showModal();
        });
        return;
      case AdsType.panel:
        Future.delayed(const Duration(seconds: 0)).then((_) {
          showPanel();
        });
        return;
      default:
        status = true;
        return;
    }
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  Uri get _url => Uri.parse(widget.launchUrl);

  @override
  Widget build(BuildContext context) {
    return _adsNormal();
  }

  Visibility _adsNormal() {
    return Visibility(
      visible: status,
      child: Padding(
        padding: widget.padding,
        child: GestureDetector(
          onTap: () {
            _launch();
            setState(() {});
          },
          child: CardAds(
            borderRadius: widget.borderRadius,
            height: widget.height,
            imageUrl: widget.imageUrl,
            child: Stack(
              children: [
                Positioned(
                  right: 0,
                  top: 0,
                  child: IconButton(
                    onPressed: () {
                      setState(() {});
                      status = false;
                    },
                    icon: const Icon(
                      Icons.close_rounded,
                      color: Colors.black,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  showPanel() {
    showModalBottomSheet<void>(
      shape: RoundedRectangleBorder(
        borderRadius: widget.borderRadius,
      ),
      context: context,
      builder: (BuildContext context) {
        return CardAds(
          height: widget.height,
          imageUrl: widget.imageUrl,
          borderRadius: const BorderRadius.only(
            topRight: Radius.circular(20),
            topLeft: Radius.circular(20),
          ),
        );
      },
    );
  }

  showModal() {
    showDialog(
      context: context,
      builder: (BuildContext context) => Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: widget.borderRadius,
        ),
        child: CardAds(
          height: widget.height,
          imageUrl: widget.imageUrl,
          borderRadius: widget.borderRadius,
        ),
      ),
    );
  }

  timerAds(Duration duration) {
    _timer = Timer.periodic(
      duration,
      (timer) {
        setState(() {});
        switch (widget.adsType) {
          case AdsType.modal:
            showModal();
            return;
          case AdsType.panel:
            showPanel();
            return;
          default:
            status = true;
            return;
        }
      },
    );
  }

  _launch() async {
    if (await canLaunchUrl(_url)) {
      await launchUrl(_url);
    } else {
      throw 'Could not launch $_url';
    }
  }
}

class CardAds extends StatelessWidget {
  const CardAds({
    Key? key,
    required this.height,
    required this.imageUrl,
    required this.borderRadius,
    this.child,
  }) : super(key: key);

  final double height;
  final String imageUrl;
  final BorderRadius borderRadius;
  final Widget? child;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      decoration: BoxDecoration(
        borderRadius: borderRadius,
        image: DecorationImage(
          fit: BoxFit.fill,
          image: NetworkImage(imageUrl),
        ),
      ),
      child: child,
    );
  }
}
