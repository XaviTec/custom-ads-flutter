import 'package:custom_ads/ads/custom_ads.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      theme: ThemeData.dark(
        useMaterial3: true,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Custom Ads'),
        ),
        body: CustomAds(
          imageUrl:
              'https://www.pollfish.com/wp-content/uploads/2017/12/Mobile_AD_FORMATS3-1.png',
          launchUrl: 'https://flutter.dev',
          borderRadius: BorderRadius.circular(20),
          adsType: AdsType.normal,
        ),
      ),
    );
  }
}
